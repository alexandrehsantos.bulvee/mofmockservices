package br.com.cipbancos.scg.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;

import javax.xml.ws.WebServiceException;

import br.com.cipbancos.scg.ws.PublishService;

public class FileUtil {

	private FileUtil() {

	}

	public static StringBuilder convertToHex(File file) throws IOException {
		StringBuilder conteudoHex = new StringBuilder();

		try (InputStream is = new FileInputStream(file)) {

			int i = 0;
			int cnt = 0;

			while ((i = is.read()) != -1) {
				if (i != -1) {
					conteudoHex.append(String.format("%02X", i));
					cnt++;
				}

				if (cnt == 16) {
					cnt = 0;
				}
			}
		} catch (Exception e) {
			throw new WebServiceException(e);
		}

		return conteudoHex;
	}

	public static File getParentPath() throws URISyntaxException {
		final Class<?> referenceClass = PublishService.class;
		final URL url = referenceClass.getProtectionDomain().getCodeSource().getLocation();
		File jarPath = null;

		jarPath = new File(url.toURI()).getParentFile();
		return jarPath;
	}

}
