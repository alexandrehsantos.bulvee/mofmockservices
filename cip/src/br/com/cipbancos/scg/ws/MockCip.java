package br.com.cipbancos.scg.ws;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.jws.WebService;
import javax.xml.ws.WebServiceException;

import br.com.cipbancos.scg.util.ConfigUtil;
import br.com.cipbancos.scg.util.ConfigUtil.SystemOS;
import br.com.cipbancos.scg.util.FileUtil;
import br.com.cipbancos.scg.util.MessageUtil;
import br.org.cipbancos.scg.webservice.ControleGarantia;
import br.org.cipbancos.scg.webservice.dto.SCGRequest;
import br.org.cipbancos.scg.webservice.dto.SCGResponse;

@WebService
public class MockCip implements ControleGarantia {

	private static final int STATUS_FINALIZADO = 0;

	@Override
	public SCGResponse executeOperation(SCGRequest scgRequest) {
		String fileContent = new String(scgRequest.getRequest());
		SCGResponse scgResponse = new SCGResponse();
		scgResponse.setConfirmation("Arquivo Recebido");
		scgResponse.setStatus(STATUS_FINALIZADO);
		scgResponse.setResponse(fileContent.getBytes());
		outputMessages(fileContent);
		return scgResponse;
	}



	private void outputMessages(String fileContent) {
		MessageUtil.resquestMessages(fileContent);
	}

	

	@Override
	public SCGResponse retryByConfirmationOperation(String confirmation) {
		return null;
	}

	@Override
	public SCGResponse retryByIdOperation(String id) {
		return null;
	}

	@Override
	public String converter(String nome) {
		StringBuilder fileToByte = fileToHexString(nome);
		return fileToByte.toString();
	}

	private StringBuilder fileToHexString(String nome) {

		try {
			File file = new File(FileUtil.getParentPath() + getPathSeparator() + nome);
			return FileUtil.convertToHex(file);
		} catch (URISyntaxException | IOException e) {
			throw new WebServiceException(e);
		}

	}

	public static String getPathSeparator() {
		SystemOS systemOS = ConfigUtil.detectOperatingSystem();

		String pathSeparator = null;

		if (systemOS.equals(SystemOS.UNIX)) {
			pathSeparator = "/";
		} else if (systemOS.equals(SystemOS.WINDOWS)) {
			pathSeparator = "\\";
		}

		return pathSeparator;
	}

}
