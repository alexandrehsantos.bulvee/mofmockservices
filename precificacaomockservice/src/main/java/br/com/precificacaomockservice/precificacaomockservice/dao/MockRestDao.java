package br.com.precificacaomockservice.precificacaomockservice.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.precificacaomockservice.precificacaomockservice.entity.MockRestJson;

@Repository
public interface MockRestDao  extends JpaRepository<MockRestJson,Long> {
	
}
