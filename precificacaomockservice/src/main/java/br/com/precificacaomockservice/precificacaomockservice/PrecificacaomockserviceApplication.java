package br.com.precificacaomockservice.precificacaomockservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrecificacaomockserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrecificacaomockserviceApplication.class, args);
	}

}

