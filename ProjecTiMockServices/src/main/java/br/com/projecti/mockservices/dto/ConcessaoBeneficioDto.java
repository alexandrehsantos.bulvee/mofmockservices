package br.com.projecti.mockservices.dto;

import java.math.BigDecimal;

public class ConcessaoBeneficioDto {
	/** The id pacote. */
	private Long idPacote;
	
	/** The nm pacote. */
	private String nmPacote;
	
	/** The id oferta. */
	private Long idOferta;
	
	/** The ds oferta. */
	private String dsOferta;
	
	/** The nm componente. */
	private String nmComponente;
	
	/** The nm beneficio. */
	private String nmBeneficio;
	
	/** The vl maximo beneficio. */
	private BigDecimal vlMaximoBeneficio;
	
	/** The tp desconto. */
	private String tpValorComponente;
	
	/** The vl desconto. */
	private BigDecimal vlDesconto;
	
	/** The qt beneficio. */
	private Integer qtBeneficio;
	
	/** The dt validade beneficio. */
	private String dtValidadeBeneficio;
	
	/**
	 * Gets the id pacote.
	 *
	 * @return the id pacote
	 */
	public Long getIdPacote() {
		return idPacote;
	}
	
	/**
	 * Sets the id pacote.
	 *
	 * @param idPacote the new id pacote
	 */
	public void setIdPacote(Long idPacote) {
		this.idPacote = idPacote;
	}
	
	/**
	 * Gets the nm pacote.
	 *
	 * @return the nm pacote
	 */
	public String getNmPacote() {
		return nmPacote;
	}
	
	/**
	 * Sets the nm pacote.
	 *
	 * @param nmPacote the new nm pacote
	 */
	public void setNmPacote(String nmPacote) {
		this.nmPacote = nmPacote;
	}
	
	/**
	 * Gets the id oferta.
	 *
	 * @return the id oferta
	 */
	public Long getIdOferta() {
		return idOferta;
	}
	
	/**
	 * Sets the id oferta.
	 *
	 * @param idOferta the new id oferta
	 */
	public void setIdOferta(Long idOferta) {
		this.idOferta = idOferta;
	}
	
	/**
	 * Gets the ds oferta.
	 *
	 * @return the ds oferta
	 */
	public String getDsOferta() {
		return dsOferta;
	}
	
	/**
	 * Sets the ds oferta.
	 *
	 * @param dsOferta the new ds oferta
	 */
	public void setDsOferta(String dsOferta) {
		this.dsOferta = dsOferta;
	}
	
	/**
	 * Gets the nm componente.
	 *
	 * @return the nm componente
	 */
	public String getNmComponente() {
		return nmComponente;
	}
	
	/**
	 * Sets the nm componente.
	 *
	 * @param nmComponente the new nm componente
	 */
	public void setNmComponente(String nmComponente) {
		this.nmComponente = nmComponente;
	}
	
	/**
	 * Gets the nm beneficio.
	 *
	 * @return the nm beneficio
	 */
	public String getNmBeneficio() {
		return nmBeneficio;
	}
	
	/**
	 * Sets the nm beneficio.
	 *
	 * @param nmBeneficio the new nm beneficio
	 */
	public void setNmBeneficio(String nmBeneficio) {
		this.nmBeneficio = nmBeneficio;
	}
	
	/**
	 * Gets the vl maximo beneficio.
	 *
	 * @return the vl maximo beneficio
	 */
	public BigDecimal getVlMaximoBeneficio() {
		return vlMaximoBeneficio;
	}
	
	/**
	 * Sets the vl maximo beneficio.
	 *
	 * @param vlMaximoBeneficio the new vl maximo beneficio
	 */
	public void setVlMaximoBeneficio(BigDecimal vlMaximoBeneficio) {
		this.vlMaximoBeneficio = vlMaximoBeneficio;
	}
	
	/**
	 * Gets the tp desconto.
	 *
	 * @return the tp desconto
	 */
	public String getTpValorComponente() {
		return tpValorComponente;
	}
	
	/**
	 * Sets the tp desconto.
	 *
	 * @param tpValorComponente the new tp desconto
	 */
	public void setTpValorComponente(String tpValorComponente) {
		this.tpValorComponente = tpValorComponente;
	}
	
	/**
	 * Gets the vl desconto.
	 *
	 * @return the vl desconto
	 */
	public BigDecimal getVlDesconto() {
		return vlDesconto;
	}
	
	/**
	 * Sets the vl desconto.
	 *
	 * @param vlDesconto the new vl desconto
	 */
	public void setVlDesconto(BigDecimal vlDesconto) {
		this.vlDesconto = vlDesconto;
	}
	
	/**
	 * Gets the qt beneficio.
	 *
	 * @return the qt beneficio
	 */
	public Integer getQtBeneficio() {
		return qtBeneficio;
	}
	
	/**
	 * Sets the qt beneficio.
	 *
	 * @param qtBeneficio the new qt beneficio
	 */
	public void setQtBeneficio(Integer qtBeneficio) {
		this.qtBeneficio = qtBeneficio;
	}
	
	/**
	 * Gets the dt validade beneficio.
	 *
	 * @return the dt validade beneficio
	 */
	public String getDtValidadeBeneficio() {
		return dtValidadeBeneficio;
	}
	
	/**
	 * Sets the dt validade beneficio.
	 *
	 * @param dtValidadeBeneficio the new dt validade beneficio
	 */
	public void setDtValidadeBeneficio(String dtValidadeBeneficio) {
		this.dtValidadeBeneficio = dtValidadeBeneficio;
	}

	@Override
	public String toString() {
		return "ConcessaoBeneficioDto [idPacote=" + idPacote + ", nmPacote=" + nmPacote + ", idOferta=" + idOferta
				+ ", dsOferta=" + dsOferta + ", nmComponente=" + nmComponente + ", nmBeneficio=" + nmBeneficio
				+ ", vlMaximoBeneficio=" + vlMaximoBeneficio + ", tpValorComponente=" + tpValorComponente
				+ ", vlDesconto=" + vlDesconto + ", qtBeneficio=" + qtBeneficio + ", dtValidadeBeneficio="
				+ dtValidadeBeneficio + "]";
	}

}
