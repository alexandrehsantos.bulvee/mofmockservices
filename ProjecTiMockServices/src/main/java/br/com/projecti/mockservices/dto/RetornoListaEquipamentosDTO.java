package br.com.projecti.mockservices.dto;

import java.util.ArrayList;
import java.util.List;

public class RetornoListaEquipamentosDTO {

	private List<Contratacao> contratacoes = new ArrayList<>();

	public List<Contratacao> getContratacoes() {
		return contratacoes;
	}

	public void setContratacoes(List<Contratacao> contratacoes) {
		this.contratacoes = contratacoes;
	}

}
