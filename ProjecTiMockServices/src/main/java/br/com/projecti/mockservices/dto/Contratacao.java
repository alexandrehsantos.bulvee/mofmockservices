package br.com.projecti.mockservices.dto;

public class Contratacao {

	private Estabelecimento estabelecimento;
	private Solucao solucoes;

	public Estabelecimento getEstabelecimento() {
		return estabelecimento;
	}

	public void setEstabelecimento(Estabelecimento estabelecimento) {
		this.estabelecimento = estabelecimento;
	}

	public Solucao getSolucao() {
		return solucoes;
	}

	public void setSolucao(Solucao solucao) {
		this.solucoes = solucao;
	}

}
