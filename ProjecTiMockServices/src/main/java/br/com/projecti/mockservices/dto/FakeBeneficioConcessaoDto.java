package br.com.projecti.mockservices.dto;

import java.util.List;

public class FakeBeneficioConcessaoDto {
	
	private String cnpj;
	private List<ConcessaoBeneficioDto> listaBeneficios;
	
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public List<ConcessaoBeneficioDto> getListaBeneficios() {
		return listaBeneficios;
	}
	public void setListaBeneficios(List<ConcessaoBeneficioDto> listaBeneficios) {
		this.listaBeneficios = listaBeneficios;
	}
	@Override
	public String toString() {
		return "FakeBeneficioConcessaoDto [cnpj=" + cnpj + ", listaBeneficios=" + listaBeneficios + "]";
	} 
}
