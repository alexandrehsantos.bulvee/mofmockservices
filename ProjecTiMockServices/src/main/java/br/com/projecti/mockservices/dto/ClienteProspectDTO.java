package br.com.projecti.mockservices.dto;

public class ClienteProspectDTO {

	String cepMaior;
	String cepMenor;

	public String getCepMaior() {
		return cepMaior;
	}

	public void setCepMaior(String cepMaior) {
		this.cepMaior = cepMaior;
	}

	public String getCepMenor() {
		return cepMenor;
	}

	public void setCepMenor(String cepMenor) {
		this.cepMenor = cepMenor;
	}

}
