package br.com.projecti.mockservices.resource;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.projecti.mockservices.dto.ClienteProspectDTO;

@RestController
@RequestMapping("trp/clienteprospectapi")
public class ClienteProspectResource {

	@GetMapping("/clientes/{cnpjCliente}/empresas-parceira/{cnpjParceiro}")
	public ResponseEntity clientProspectParametrosEligibilidade(@PathVariable Long cnpjCliente, @PathVariable Long cnpjParceiro) {
		
		ClienteProspectDTO clienteProspectDTO = new ClienteProspectDTO();
		clienteProspectDTO.setCepMaior("07000001");
		clienteProspectDTO.setCepMenor("09079999");
		return ResponseEntity.ok(clienteProspectDTO);
	}

}
