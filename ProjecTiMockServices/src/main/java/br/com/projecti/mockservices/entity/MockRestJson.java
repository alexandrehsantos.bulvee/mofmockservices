package br.com.projecti.mockservices.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "MOCK_REST_JSON")
public class MockRestJson implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 669283799397881426L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "seqMockConcessao")
	@SequenceGenerator(name = "seqMockConcessao", sequenceName = "SEQ_MOCK_CONCESSAO", allocationSize = 1, initialValue = 1)
	@Column(name = "id_json")
	private Long id;

	@Column(name = "id_oferta")
	private Long idOferta;

	@Column(name = "json_body")
	private String json;

	@Column(name = "dt_created")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCriacao;

	@Column(name = "CALLER_DESCRIPTION")
	private String callerDescription;
	
	@Column(name = "URL_SERVICO_MOCK")
	private String urlMock;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdOferta() {
		return idOferta;
	}

	public void setIdOferta(Long idOferta) {
		this.idOferta = idOferta;
	}

	public String getJson() {
		return json;
	}

	public void setJson(String json) {
		this.json = json;
	}

	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	/**
	 * @return the callerDescription
	 */
	public String getCallerDescription() {
		return callerDescription;
	}

	/**
	 * @param callerDescription
	 *            the callerDescription to set
	 */
	public void setCallerDescription(String callerDescription) {
		this.callerDescription = callerDescription;
	}

	public String getUrlMock() {
		return urlMock;
	}

	public void setUrlMock(String urlMock) {
		this.urlMock = urlMock;
	}

}
